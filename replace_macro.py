#!/usr/bin/env python3
import re
import sys

macros=list()
names=list()
bodies=list()
argnames=list()
if(len(sys.argv)<2):
   print("usage: replace_macro.py filename macro1 [macro2 ... ]")

for macro in sys.argv[2:]:
   p = re.split("\s*\#define\s+",macro)
   if p[0]!='' or len(p)<2:
      print("Invalid macro definiton:",macro) 
      exit(0)
   p=re.split("\s*\)\s+",p[1],1)
   if(len(p)!=2):
      print("Invalid macro definiton:",macro) 
      exit(0)
   body=p[1]
   p=re.split('\s*\(s*',p[0])
   if(len(p)!=2):
      print("Invalid macro definiton:",macro) 
      exit(0)
   name=p[0]
   argname=p[1]
   macros.append(macro)
   bodies.append(body)
   argnames.append(argname)
   names.append(name)


f = open(sys.argv[1], "r")
content=f.read()
f.close()
lines=content.split('\n')
changed=False

for i in range(len(lines)):
   for j in range(len(macros)):
      macro=macros[j]
      body=bodies[j]
      argname=argnames[j]
      name=names[j]
      line=lines[i]
      find=name+"\s*\([a-zA-Z0-9_]+\)"
      r=re.search(find,line)
      if(r): 
         replacestr=line[r.start():r.end()]
         replacearg=replacestr.split('(')[1][:-1]
         replacetype=body.replace(argname.strip(),replacearg.strip())
         newline=line.replace(replacestr,replacetype)
         lines[i]=newline
         changed=True
if(changed):
    f = open(sys.argv[1], "w")
    for l in lines:
        print(l,file=f)
    f.close()


